
let courseForm = document.querySelector("#createCourse")

courseForm.addEventListener("submit", (e) => {
	e.preventDefault() 

let courseName = document.querySelector("#courseName").value
let courseDescription = document.querySelector("#courseDescription").value
let coursePrice = document.querySelector("#coursePrice").value


// document.getElementById("courseName").innerHTML = courseName
// document.getElementById("courseDescription").innerHTML = courseDescription
// document.getElementById("coursePrice").innerHTML = coursePrice


console.log(courseName)
console.log(courseDescription)
console.log(coursePrice)


if(courseName == "" || courseDescription == "" || coursePrice == "") {
        alert("Please complete all fields!")
} else { 
	fetch('http://localhost:4000/api/courses/course-exists', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
                name: courseName
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === false){
                fetch('http://localhost:4000/api/courses', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name: courseName,
                        description: courseDescription,
                        price : coursePrice,
                    })
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    console.log(data)
                    
                    if(data === true){
                        alert("New Course has been added")
                        window.location.replace("./courses_admin.html")
                    } else {
                        alert("Something Went Wrong in the Registration!")
                    }
                })
            }
        })

}



})

