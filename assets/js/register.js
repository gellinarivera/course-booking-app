// for testing
// console.log("hello from register")

let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault() //to avoid multiple request

let firstName = document.querySelector("#firstName").value
let lastName = document.querySelector("#lastName").value
let email = document.querySelector("#userEmail").value
let mobileNo = document.querySelector("#mobileNumber").value
let password1 = document.querySelector("#password1").value
let password2 = document.querySelector("#password2").value

//for testing
console.log(firstName)
console.log(lastName)
console.log(email)
console.log(mobileNo)
console.log(password1)
console.log(password2)

//validation for submit button when all fields are registered and if both passwords match
	if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)) {

        //if all the requirements are met, then now lets check for duplicate emails in the database first. 
        fetch('https://frozen-bastion-92334.herokuapp.com/api/users/email-exists', {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            //if no email duplicates are found 
            if(data === false){
                fetch('https://frozen-bastion-92334.herokuapp.com/api/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email : email,
                        password: password1,
                        mobileNo: mobileNo
                    })
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    console.log(data)
                    // lets give a proper response if registration will become successful
                    if(data === true){
                        alert("New Account Registered successfully")
                        //redirect to login 
                        window.location.replace("./login.html")
                    } else {
                        alert("Something Went Wrong in the Registration!")
                    }
                })
            }
        })
    } else {
      alert("Something went wrong, check credentials!")  
    }
})
